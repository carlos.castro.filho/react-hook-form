import styled from "styled-components" ;

const Input = styled.input`
  padding: 10px;
  font-size: inherit;
  width: 95%;
  height: 100%;

`;

export default Input ;