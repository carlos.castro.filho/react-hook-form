import styled from "styled-components" ;

const Label = styled.label.attrs( props=> ({
  size: props.width || "100%",
  altura: props.height || "100%",
})) `
  display: flex ;
  flex-direction: column;
  font-size: 30px;
  align-items:center;
  margin-bottom: 20px;
  width:  ${props => props.size};
  height :  ${props => props.altura};

`;

export default Label ;