import React from "react" ;

import Label from "../Label" ;
import Content from "../content";
import Input from "../Input" ;

const PassWord = ({ label, name, register , width }) => (
    <Label width={width} >
       <Content>{ label }</Content>
       <Input type="password"
        name={name}
        {...register(name)} />
    </Label>
);

export default PassWord;