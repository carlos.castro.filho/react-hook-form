import React from "react" ;

import Label from "../Label" ;
import Content from "../content";
import Input from "../Input" ;

const TextArea = ({ label, name, register , width, height }) => (
    <Label width={width} height={height} >
       <Content>{ label }</Content>
       <Input type="textArea"
        name={name}
        {...register(name)} />
    </Label>
);

export default TextArea;