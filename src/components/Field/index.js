import React from "react" ;

import PassWord from "./Password";
import TextArea from "./TextArea";
import Text from "./Text" ;
import Date from "./Date";
import Mask from "./Mask" ;

const Field = {
    Text,
    Date,
    PassWord,
    TextArea,
    Mask,
};

export default Field;