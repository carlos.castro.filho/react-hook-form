import React from "react" ;
import { useForm } from "react-hook-form";
import { yupResolver} from "@hookform/resolvers/yup" ;

import  * as S  from "./sytles";
import Form from "../Form";
import Field from "../Field";
import Button from "../button";
import schema from "./validation" ;

const FormNew = () => {

      const { register, handleSubmit, formState:{ errors } } = useForm({
      resolver: yupResolver(schema),

   });

   const newUser = (data) => {
       console.log(JSON.stringify(data, null, 4));

   };

   return ( 
      <S.Container>
     <Form width="100rem" onSubmit={handleSubmit(newUser)}>
        <Field.Text width="100%" label="Nome" name="name" type="text" register={register} />
        <p>{errors.name?.message }</p>

        <Field.Text width="400px" label="Email" name="email" type="email" register={register} />
        <p>{errors.email?.message }</p>

        <Field.PassWord width="300px" label="Senha" name="password" register={register} />
        

        <Field.TextArea height="200px" label="Observação:" name="observacao" register={register} />
        

        <Field.Date width="250px" label="Dia:" name="dia" register={register} />

        <Field.Mask width="250px" label="Telefone:" name="tel_1" register={register} />
        

        <Button>enviar</Button>

     </Form>
     </S.Container>
   );
};

export default FormNew ;