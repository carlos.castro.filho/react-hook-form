import * as yup from "yup" ;

const schema = yup.object().shape({
    name: yup
    .string()
    .min(2,"minimo 2 caracteres")
    .required("nome e obrigatório"),
    email: yup
    .string()
    .email(" Digite um email válido")
    .required("email é obrigatório"),
 })


 export default schema ;