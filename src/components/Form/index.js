import styled from "styled-components" ;

const Form = styled.form.attrs( props=> ({
   size: props.width || "100%",
})) `
  width: ${props => props.size};
  max-width: 600px;
  //background-color: red ;
  padding: 10px ;

`;

export default Form ;