import React from "react" ;
import { useForm } from "react-hook-form";
import { yupResolver} from "@hookform/resolvers/yup" ;

import Field from "../Field";
import Button from "../button";
import schema from "./validation" ;
import FormLoginSignup from "../FormLoginSignup";
import Title from "../Title"
import Main from "../Main";


const FormLogin = () => {

      const { register, handleSubmit, formState:{ errors } } = useForm({
      resolver: yupResolver(schema),

   });

   const newUser = (data) => {
       console.log(JSON.stringify(data, null, 4));

   };

   return ( 
     <Main>
     <Title> Login </Title>
     <FormLoginSignup onSubmit={handleSubmit(newUser)}>
        <Field.Text width="100%" label="Nome" name="name" type="text" register={register} />
        <p>{errors.name?.message }</p>
   

        <Field.Text width="400px" label="Email" name="email" type="email" register={register} />
        <p>{errors.email?.message }</p>
        <Button>enviar</Button>

     </FormLoginSignup>
     </Main> 
   );
};

export default FormLogin ;