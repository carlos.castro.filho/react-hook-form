import styled from "styled-components" ;
import Box from "../Box";

// utilizando component generico Box, convertendo para form
const FormLoginSignup = styled(Box).attrs( { as:"form"}) `
width: 600px;
height: 480px;
display:flex;
background-color: #fff;

`;

export default FormLoginSignup;