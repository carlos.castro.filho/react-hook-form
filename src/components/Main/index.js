import styled from "styled-components" ;
import Button from "../button";
import Form from "../Form";

const Main = styled.main`
   display: flex;
   justify-content: center ;
   align-items: center ;

   background-color: #081229;
   // define que o main vai ocpupar toda a tela
   // vh = view port height ( memos scroll)
   height: 100vh;
   //
   width: 100vw;

   // para acessar um outro compoment stled
   & > ${Button} {
      background-color: red ;
   }

   & > ${ Form }{
     // background-color: yellow;
      padding: 20 px ;
      width: 700px;

   }

`;

export default Main ;